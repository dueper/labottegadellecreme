<?php 
/*
Plugin Name: WooCommerce Partita IVA e Codice Fiscale [FREE]
Plugin URI: http://www.netstarsolution.net/woocommerce-partita-iva-e-codice-fiscale-free/
Description: Grazie a questo plugin si potrà rendere compatibile il vostro woocommerce con il mercato italiano grazie all'aggiunta dei campi codice fiscale/partita iva. Questi campi veranno salvati e veranno visualizzati sia nell'ordine che nell'email del cliente. [VERSIONE FREE]
Version: 1.0
Author: NetstarSolution
Author URI: http://netstarsolution.net
*/

add_action( 'woocommerce_after_order_notes', 'campo_cf_piva' );
 
function campo_cf_piva( $checkout ) {
 
    echo '<div id="my_custom_checkout_field"><h2>' . __('Campo relativo alla fattura') . '</h2>';
 
    woocommerce_form_field( 'woocommerce_cf_piva', array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => __('Cod. Fiscale/P. Iva <abbr class="required" title="obbligatorio">*</abbr>'),
        'placeholder'   => __(''),
        ), $checkout->get_value( 'woocommerce_cf_piva' ));
 
    echo '</div>';
 
}


add_action('woocommerce_checkout_process', 'controllo_campo_cf_piva');
 
function controllo_campo_cf_piva() {
    if ( ! $_POST['woocommerce_cf_piva'] || strlen($_POST['woocommerce_cf_piva']) < 9 )
        wc_add_notice( __( 'Inserisci correttamente il Cod. Fiscale o la P. Iva.' ), 'error' );
}


add_action( 'woocommerce_checkout_update_order_meta', 'order_meta_campo_cf_piva' );
 
function order_meta_campo_cf_piva( $order_id ) {
    if ( ! empty( $_POST['woocommerce_cf_piva'] ) ) {
        update_post_meta( $order_id, 'campo_cf_piva', sanitize_text_field( $_POST['woocommerce_cf_piva'] ) );
    }
}


add_action( 'woocommerce_admin_order_data_after_billing_address', 'edit_order_campo_cf_piva', 10, 1 );
 
function edit_order_campo_cf_piva($order){
    echo '<p><strong>'.__('Cod. Fiscale/P. Iva').':</strong> ' . get_post_meta( $order->id, 'campo_cf_piva', true ) . '</p>';
}

add_action('woocommerce_order_details_after_order_table', 'info_order');

function info_order($order_id){
	$order_object = new WC_Order( $order_id );
    if(get_post_meta( $order_id->id , 'campo_cf_piva', true)!='') {echo '<p><strong>Cod. Fiscale/P. Iva:</strong>' . get_post_meta( $order_id->id , 'campo_cf_piva', true) . '</p>';}
}


add_action('woocommerce_email_after_order_table', 'info_order_email');

function info_order_email($order_id){
    $order_object = new WC_Order( $order_id );
    if(get_post_meta( $order_id->id , 'campo_cf_piva', true)!='') {echo '<p><strong>Cod. Fiscale/P. Iva:</strong>' . get_post_meta( $order_id->id , 'campo_cf_piva', true) . '</p>';}
}


?>