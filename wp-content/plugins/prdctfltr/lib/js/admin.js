(function($){
"use strict";

	var indexBefore = -1;

	function getIndex(itm, list) {
		var i;
		for (i = 0; i < list.length; i++) {
			if (itm[0] === list[i]) break;
		}
		return i >= list.length ? -1 : i;
	}

	
	$('.prdctfltr_customizer').sortable({
		cursor:'move',
		start: function(event, ui) {
			indexBefore = getIndex(ui.item, $('.prdctfltr_customizer > span'));
		},
		stop: function(event, ui) {

			var count = $('.prdctfltr_customizer span.adv').length;

			if ( count > 0 ) {
				var i = 0;
				$('.prdctfltr_customizer span.adv').each( function() {
					var curr_el = $(this);
					var curr = curr_el.attr('data-id');

					curr_el.find('[name]').each( function() {
						var attr = $(this).attr('name');
						$(this).attr('name', attr.replace('['+curr+']', '['+i+']'))
					});

					curr_el.attr('data-id', i);

					i++;
				});

			}


			var indexAfter = getIndex(ui.item,$('.prdctfltr_customizer > span'));

			if (indexBefore==indexAfter) {
				return;
			} else {
				if (indexBefore<indexAfter) {
					$($('#wc_settings_prdctfltr_active_filters option')[indexBefore]).insertAfter($($('#wc_settings_prdctfltr_active_filters option')[indexAfter]));
				}
				else {
					$($('#wc_settings_prdctfltr_active_filters option')[indexBefore]).insertBefore($($('#wc_settings_prdctfltr_active_filters option')[indexAfter]));
				}
			}


		}
	});

	$(document).on('click', '.prdctfltr_c_visible', function() {
		var curr_el = $(this).parent();

		var curr_index = getIndex(curr_el, $('.prdctfltr_customizer > span'));

		if ( curr_el.find('.prdctfltr-eye').length > 0 ) {
			curr_el.find('.prdctfltr-eye').removeClass('prdctfltr-eye').addClass('prdctfltr-eye-disabled');
			$('#wc_settings_prdctfltr_active_filters option').eq(curr_index).prop("selected", false);
		}
		else {
			curr_el.find('.prdctfltr-eye-disabled').removeClass('prdctfltr-eye-disabled').addClass('prdctfltr-eye');
			$('#wc_settings_prdctfltr_active_filters option').eq(curr_index).prop("selected", true);
		}
		return false;
	});

	$(document).on('click', '.prdctfltr_c_delete', function() {
		var curr_el = $(this).parent();

		var curr_index = getIndex(curr_el, $('.prdctfltr_customizer > span'));

		$('#wc_settings_prdctfltr_active_filters option').eq(curr_index).remove();

		$('.prdctfltr_c_add_filter[data-filter='+curr_el.attr('data-filter')+']').removeClass('pf_active').find('i').removeClass('prdctfltr-eye').addClass('prdctfltr-eye-disabled');

		curr_el.remove();

		return false;
	});

	$(document).on('change', '.prdctfltr_adv_select', function() {

		var curr_el = $(this).parent();
		var curr = curr_el.closest('.adv').attr('data-id');

		var curr_data = {
			action: 'prdctfltr_c_terms',
			taxonomy: $(this).find('option:selected').attr('value')
		};

		$.post(prdctfltr.ajax, curr_data, function(response) {
			if (response) {
				response = response.replace(/\%%/g, curr);
				curr_el.next().replaceWith(response);
			} else { 
				alert('Error!');
			}
		});

	});

	$(document).on('click', '.prdctfltr_c_add_filter', function() {
		var curr_el = $(this);
		
		if ( curr_el.hasClass('pf_active') ) {

			if ( confirm('Deactivate?') === false ) {
				return;
			}

			$('#wc_settings_prdctfltr_active_filters option[value='+curr_el.attr('data-filter')+']').remove();
			$('.prdctfltr_customizer span[data-filter='+curr_el.attr('data-filter')+']').remove();

			curr_el.removeClass('pf_active').find('i').removeClass('prdctfltr-eye').addClass('prdctfltr-eye-disabled');

		}
		else {

			if ( confirm('Activate?') === false ) {
				return;
			}

			$('#wc_settings_prdctfltr_active_filters').append('<option value="'+curr_el.attr('data-filter')+'" selected="selected">'+curr_el.find('span').text()+'</option>');
			$('.prdctfltr_customizer').append('<span class="pf_element" data-filter="'+curr_el.attr('data-filter')+'"><span>'+curr_el.find('span').text()+'</span><a href="#" class="prdctfltr_c_delete"><i class="prdctfltr-delete"></i></a><a href="#" class="prdctfltr_c_move"><i class="prdctfltr-move"></i></a></span>');

			curr_el.addClass('pf_active').find('i').removeClass('prdctfltr-eye-disabled').addClass('prdctfltr-eye');

		}
		
		return false;
	});

	$(document).on('click', '.prdctfltr_c_add', function() {

		var curr_el = $(this).parent().next();
		var curr = curr_el.find('.pf_element.adv').length;

		var curr_data = {
			action: 'prdctfltr_c_fields'
		};

		$.post(prdctfltr.ajax, curr_data, function(response) {
			if (response) {
				response = response.replace(/\%%/g, curr);

				var adv_ui = '<span class="pf_element adv" data-filter="advanced" data-id="'+curr+'"><span>Advanced Filter</span><a href="#" class="prdctfltr_c_delete"><i class="prdctfltr-delete"></i></a><a href="#" class="prdctfltr_c_move"><i class="prdctfltr-move"></i></a><span class="pf_options_holder">'+response+'</span></span>';
				$('#wc_settings_prdctfltr_active_filters').append('<option value="advanced" selected="selected">Advanced Filter</option>');

				var curr_append = curr_el.append(adv_ui);

			} else { 
				alert('Error!');
			}
		});

		return false;
	});


	function makeVals(formControl, controlType, value) {

		switch (controlType) {
			case 'text':
				$(formControl).val(value);
			break;
			case 'number':
				$(formControl).val(value);
			break;
			case 'textarea':
				$(formControl).val(value);
			break;
			case 'radio':
				$(formControl).val(value);
			break;
			case 'checkbox':
				$(formControl).prop('checked', ( value == 'yes' ? true : false ));
				break;
			case 'select':
				$(formControl).val(value);
				break;
			case 'multiselect':
				if ( value !== null ) {
					$(formControl).val(value);
				}
				break;
		}
		return;
	}


	function getVals(formControl, controlType) {

		switch (controlType) {
			case 'text':
				var value = $(formControl).val();
			break;
			case 'number':
				var value = $(formControl).val();
			break;
			case 'textarea':
				var value = $(formControl).val();
			break;
			case 'radio':
				var value = $(formControl).val();
			break;
			case 'checkbox':
				if ($(formControl).is(":checked")) {
					value = 'yes';
				}
				else {
					value = 'no';
				}
				break;
			case 'select':
				var value = $(formControl).val();
				break;
			case 'multiselect':
				var value = $(formControl).val() || [];
				break;
		}
		return value;
	}


	$(document).on('click', '.prdctfltr_or_add', function() {

		if ( confirm('Add override?') === false ) {
			return false;
		};

		var curr = $(this).closest('p');

		var curr_data = {
			action: 'prdctfltr_or_add',
			curr_tax: curr.attr('class'),
			curr_term: curr.find('.prdctfltr_or_select').val(),
			curr_override: curr.find('.prdctfltr_filter_presets').val()
		};

		if ( curr_data.curr_term == undefined || curr_data.curr_override == 'default' ) {
			alert('Please select both term and filter preset.');
			return;
		}

		$.post(prdctfltr.ajax, curr_data, function(response) {
			if (response) {
				curr.prepend('<span class="prdctfltr_override">Term slug : <span class="slug">'+curr.find('.prdctfltr_or_select').val()+'</span><br>Filter Preset : '+curr.find('.prdctfltr_filter_presets').val()+' <a href="#" class="button prdctfltr_or_remove">Remove Override</a><span class="clearfix"></span></span>')
				alert('Added');
			} else { 
				alert('Error!');
			}
		});

		return false;
	});

	$(document).on('click', '.prdctfltr_or_remove', function() {

		if ( confirm('Remove override?') === false ) {
			return false;
		};

		var curr = $(this).closest('p');

		var curr_data = {
			action: 'prdctfltr_or_remove',
			curr_tax: curr.attr('class'),
			curr_term: $(this).parent().find('.slug').text()
		};

		$.post(prdctfltr.ajax, curr_data, function(response) {
			if (response) {
				curr.find('span.prdctfltr_override').remove();
				alert('Removed');
			} else {
				alert('Error!');
			}
		});

		return false;
	});

	$(document).on('click', '#prdctfltr_save', function() {

		var curr_saving = {};

		var inputs = $('input[name^=wc_settings_prdctfltr], select[name^=wc_settings_prdctfltr], textarea[name^=wc_settings_prdctfltr]'), tmp;
		$.each(inputs, function(i, obj) {
			var tag = ( $(obj).prop('tagName') == 'INPUT' ? $(obj).attr('type') : $(obj).prop('tagName').toLowerCase() );
			curr_saving[$(obj).attr('name').replace('[]', '')] = getVals($(obj), tag);
		});

		var i = 0;

		var curr_adv = {};

		curr_adv['pfa_title'] = [];
		curr_adv['pfa_taxonomy'] = [];
		curr_adv['pfa_include'] = [];
		curr_adv['pfa_multiselect'] = [];
		curr_adv['pfa_adoptive'] = [];

		$('.pf_element.adv').each( function() {
			var curr_el = $(this);
			curr_adv['pfa_title'][i] = curr_el.find('input[name^="pfa_title"]').val();
			curr_adv['pfa_taxonomy'][i] = curr_el.find('select[name^="pfa_taxonomy"] option:selected').val();
			curr_adv['pfa_include'][i] = curr_el.find('select[name^="pfa_include"]').val();
			curr_adv['pfa_multiselect'][i] = ( curr_el.find('input[name^="pfa_multiselect"]:checked').length > 0 ? 'yes' : 'no' );
			curr_adv['pfa_adoptive'][i] = ( curr_el.find('input[name^="pfa_adoptive"]:checked').length > 0 ? 'yes' : 'no' );
			i++;
		});

		curr_saving['wc_settings_prdctfltr_advanced_filters'] = curr_adv;

		var curr_name = prompt('Enter template name to save it');

		if ( curr_name == '' || curr_saving == '' ) {
			alert('Missing name or settings.');
			return false;
		}
		if ( curr_name === null ) {
			return false;
		}

		var curr_data = {
			action: 'prdctfltr_admin_save',
			curr_name: curr_name,
			curr_settings: JSON.stringify(curr_saving)
		};

		$.post(prdctfltr.ajax, curr_data, function(response) {
			if (response) {
				$('select#prdctfltr_filter_presets').append('<option value="'+curr_data.curr_name+'">'+curr_data.curr_name+'</option>');
				alert('Saved');
			} else {
				alert('Error!');
			}
		});
		
		return false;

	});


	$(document).on('click', '#prdctfltr_load', function() {

		if ( confirm('Load?') === false ) {
			return false;
		};

		var curr_data = {
			action: 'prdctfltr_admin_load',
			curr_name: $('select#prdctfltr_filter_presets option:selected').val()
		};

		if ( curr_data.curr_name == '' || curr_data.curr_name == 'default' ) {
			alert('Not selected.');
			return false;
		}

		$.post(prdctfltr.ajax, curr_data, function(response) {
			if (response) {
				var curr_preset = $.parseJSON(response);

				var inputs = $('input[name^=wc_settings_prdctfltr], select[name^=wc_settings_prdctfltr], textarea[name^=wc_settings_prdctfltr]'), tmp;
				$.each(inputs, function(i, obj) {
					var tag = ( $(obj).prop('tagName') == 'INPUT' ? $(obj).attr('type') : $(obj).prop('tagName').toLowerCase() );
					if ( tag == 'select' && $(obj).prop('multiple') == true ) {
						tag = 'multiselect';
					}
					makeVals($(obj), tag, curr_preset[$(obj).attr('name').replace('[]','')]);
				});

				if ( curr_preset['wc_settings_prdctfltr_active_filters'] !== undefined ) {
					var curr_el = $('.prdctfltr_customizer');
					var curr_flds = $('.prdctfltr_customizer_fields');

					curr_el.empty();
					curr_flds.find('a.prdctfltr_c_add_filter:not(.pf_advanced)').removeClass('pf_active');
					curr_flds.find('a.prdctfltr_c_add_filter:not(.pf_advanced) i').removeAttr('class').addClass('prdctfltr-eye-disabled');

					$('#wc_settings_prdctfltr_active_filters').empty();

					var curr=0,purr = 0;
					$.each(curr_preset['wc_settings_prdctfltr_active_filters'], function(index, pf_filter) {
						$('#wc_settings_prdctfltr_active_filters').append('<option value="'+pf_filter+'" selected="selected">'+pf_filter+'</option>');

						if ( pf_filter == 'advanced' ) {

							var curr_set = curr_preset['wc_settings_prdctfltr_advanced_filters'];

							curr_el.append('<span class="pf_element adv" data-filter="'+pf_filter+'" data-id="'+purr+'"><span>Advanced Filter</span><a href="#" class="prdctfltr_c_delete"><i class="prdctfltr-delete"></i></a><a href="#" class="prdctfltr_c_move"><i class="prdctfltr-move"></i></a><span class="pf_options_holder"></span></span>');

							var curr_data = {
								action: 'prdctfltr_c_fields',
								pfa_title: curr_set['pfa_title'][purr],
								pfa_taxonomy: curr_set['pfa_taxonomy'][purr],
								pfa_include: curr_set['pfa_include'][purr],
								pfa_multiselect: curr_set['pfa_multiselect'][purr],
								pfa_adoptive: curr_set['pfa_adoptive'][purr]
							};

							$.post(prdctfltr.ajax, curr_data, function(response) {
								if (response) {
									response = response.replace(/\%%/g, curr);
									curr_el.find('.pf_element.adv').eq(curr).find('.pf_options_holder').append(response);
									curr++;
								} else {
									alert('Error!');
								}
							});
							purr++;
						}
						else {
							curr_flds.find('a.prdctfltr_c_add_filter[data-filter="'+pf_filter+'"]').addClass('pf_active');
							curr_flds.find('a.prdctfltr_c_add_filter[data-filter="'+pf_filter+'"] i').removeAttr('class').addClass('prdctfltr-eye');
							curr_el.append('<span class="pf_element" data-filter="'+pf_filter+'"><span>'+curr_flds.find('a.prdctfltr_c_add_filter[data-filter="'+pf_filter+'"] span').text()+'</span><a href="#" class="prdctfltr_c_delete"><i class="prdctfltr-delete"></i></a><a href="#" class="prdctfltr_c_move"><i class="prdctfltr-move"></i></a></span>');
						}
						
					});
					
				}

				alert('Loaded');
			} else {
				alert('Error!');
			}
		});

		return false;

	});
	$(document).on('click', '#prdctfltr_delete', function() {

		if ( confirm('Delete?') === false ) {
			return false;
		};

		var curr_data = {
			action: 'prdctfltr_admin_delete',
			curr_name: $('select#prdctfltr_filter_presets option:selected').val()
		};

		if ( curr_data.curr_name == '' ) {
			alert('Not selected.');
			return false;
		}

		$.post(prdctfltr.ajax, curr_data, function(response) {
			if (response) {
				$('select#prdctfltr_filter_presets option[value="'+curr_data.curr_name+'"]').remove();
				alert('Deleted');
			} else {
				alert('Error!');
			}
		});

		return false;

	});
	
	
	$('#wc_settings_prdctfltr_selected, #wc_settings_prdctfltr_attributes').closest('tr').hide();


})(jQuery);