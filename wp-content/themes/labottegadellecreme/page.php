<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @author Dueper Design
 * @package Dueper Theme
 */

get_header();
if(have_posts()): the_post();?>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-10 col-lg-offset-1 page-content">
            <?php /* if(!is_cart() && !is_checkout() && !is_account_page()): ?>
                <h1><?php the_title(); ?></h1>
            <?php endif; */ ?>
            <?php the_content(); ?>
        </div><!--col-md-10-->
    </div>
</div><!--container-->

<?php endif;
get_footer(); ?>