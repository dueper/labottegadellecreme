<?php
/**
 *
 * @author Dueper Design
 * @package Dueper Theme
 */

if (!isset($content_width)) $content_width = 770;

/**
 * upbootwp_setup function.
 *
 * @access public
 * @return void
 */
function upbootwp_setup() {

	require 'inc/general/class-Upbootwp_Walker_Nav_Menu.php';
	require 'inc/general/class-Mobile_Walker_Nav_Menu.php';

	load_theme_textdomain('upbootwp', get_template_directory().'/languages');

	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	register_nav_menus( array(
		'primary' => __( 'Menu generale', 'Menu Generale' ),
        'user' => __( 'Menu Profilo', 'Menu con i link del profilo' ),
        'footer' => __( 'Footer Menu', 'Menu visualizzato sul footer' ),
        'mobile' => __( 'Mobile Menu', 'Menu visualizzato sui dispositivi mobile' )
	) );


	/**
	 * Enable support for Post Formats
	 */
	//add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

	/**
	 * Setup the WordPress core custom background feature.
	 */
	/*add_theme_support( 'custom-background', apply_filters( 'upbootwp_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	)));*/


}

add_action( 'after_setup_theme', 'upbootwp_setup' );


/**
 * Creo le dimensioni per le varie thumbnail
 */
if ( function_exists( 'add_theme_support' ) ) {
  add_theme_support( 'post-thumbnails' );
  set_post_thumbnail_size( 160, 120 ); // 160 pixels wide by 120 pixels high
}
if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'single-product', 500, 500, true );
	add_image_size( 'bg-fascia', 1920, 1080, true );
    add_image_size( 'prodotto-tall', 380, 580, true );
}


/**
 * Register widgetized area and update sidebar with default widgets
 */
function upbootwp_widgets_init() {
	register_sidebar(array(
		'name'          => __('Sidebar','upbootwp'),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	));
	register_sidebar(array(
		'name'          => __('Sidebar footer sx','upbootwp'),
		'id'            => 'sidebar-footer-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	));
	register_sidebar(array(
		'name'          => __('Sidebar footer dx','upbootwp'),
		'id'            => 'sidebar-footer-2',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	));
    register_sidebar(array(
		'name'          => __('Sidebar Filter 1','upbootwp'),
		'id'            => 'sidebar-filter-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	));
    register_sidebar(array(
		'name'          => __('Sidebar Filter 2','upbootwp'),
		'id'            => 'sidebar-filter-2',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	));

}
add_action( 'widgets_init', 'upbootwp_widgets_init' );


################################################################################
// Loading All CSS Stylesheets
################################################################################
function bootstrapwp_css_loader() {
	wp_enqueue_style('bootstrapwp', get_template_directory_uri().'/css/bootstrapwp.css', false ,'0.90', 'all' );
	wp_enqueue_style('style', get_template_directory_uri().'/style.css');

    wp_enqueue_style('woocommerce-custom', get_template_directory_uri().'/woocommerce-custom.css');

    /* RESPONSIVE */
    wp_enqueue_style('small-device', get_template_directory_uri().'/css/small-device.css', false );
    wp_enqueue_style('mini-device', get_template_directory_uri().'/css/mini-device.css', false );

	/* MMENU */
	wp_enqueue_style('mmenu', get_template_directory_uri().'/js/mmenu/css/jquery.mmenu.all.css', false);

	/* ICONFONT */
	//wp_enqueue_style('iconfont', get_template_directory_uri().'/css/iconfont/css/picto-foundry-general.css');
	//wp_enqueue_style('iconfont-nature', get_template_directory_uri().'/css/iconfont/css/picto-foundry-nature.css');

	/* NEWS TICKER */
	//wp_enqueue_style('newsticker', get_template_directory_uri().'/js/newsticker/styles/ticker-style.css');

	/* GOOGLE FONT */
	wp_enqueue_style('Googlefonts', 'http://fonts.googleapis.com/css?family=Lato:300,400,900,300italic,400italic,900italic');

	/* OWL CAROUSEL */
	wp_enqueue_style('owl-carousel', get_template_directory_uri().'/js/owl-carousel/owl.carousel.css');
	wp_enqueue_style('owl-carousel', get_template_directory_uri().'/js/owl-carousel/owl.theme.default.css');

	/* ANIMATE CSS */
	//wp_enqueue_style('animate-css', get_template_directory_uri().'/css/animate.css');

	/* FLEXSLIDER */
	wp_enqueue_style('flexslider', get_template_directory_uri().'/js/flexslider/flexslider.css');

	/* FRESCO LIGHTBOX */
	//wp_enqueue_style('fresco', get_template_directory_uri().'/js/fresco/fresco.css');

	/* TOOLTIPSTER */
	//wp_enqueue_style('tooltipster', get_template_directory_uri().'/js/tooltipster/css/tooltipster.css');

	/* ICONMOON */
	wp_enqueue_style('bottegafont', get_template_directory_uri().'/fonts/bottegafont/bottegafont.css');
}
add_action('wp_enqueue_scripts', 'bootstrapwp_css_loader');

################################################################################
// Loading all JS Script Files.  Remove any files you are not using!
################################################################################
function bootstrapwp_js_loader() {
	wp_enqueue_script( 'upbootwp-basefile', get_template_directory_uri().'/js/bootstrap.min.js',array(),'20130905',true);
	wp_enqueue_script( 'dropdown-hover', get_template_directory_uri().'/js/bootstrap-hover-dropdown.min.js',array('jquery'),'0.90', true );
	wp_enqueue_script( 'jquery-easing', '//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js',array('jquery'));

	/* MMENU */
	wp_enqueue_script( 'mmenu-js', get_template_directory_uri().'/js/mmenu/js/jquery.mmenu.min.all.js','','2.0',true);

	/* SHARRRE */
	wp_enqueue_script( 'sharrre', get_template_directory_uri().'/js/jquery.sharrre.min.js',array('jquery'),'1.7', true );

	/* NUMBER ANIMATION */
	//wp_enqueue_script( 'animate-number', get_template_directory_uri().'/js/jquery.animateNumber.js');

	/* MASONRY */
	//wp_enqueue_script('masonry', get_template_directory_uri().'/js/masonry.pkgd.min.js');

	/* Infinite Scroll */
	//wp_enqueue_script('infinitescroll', get_template_directory_uri().'/js/jquery.infinitescroll.js', array('jquery'),'1.0', false );

    /* Equal Height */
	wp_enqueue_script('equalheight', get_template_directory_uri().'/js/jquery.matchHeight.min.js', array('jquery'),'1.0', false );

	//wp_enqueue_script('imagesloaded', get_template_directory_uri().'/js/imagesloaded.js');

	/* GMAPS */
	wp_enqueue_script('gmaps', get_template_directory_uri().'/js/gmaps.js');

    /* adaptive-backgrounds */
	wp_enqueue_script('adaptive-backgrounds', get_template_directory_uri().'/js/jquery.adaptive-backgrounds.js');

    /* spin js */
	wp_enqueue_script('spin', get_template_directory_uri().'/js/spin.min.js');

	/* STYKY NAV*/
	wp_enqueue_script( 'stiky', get_template_directory_uri().'/js/jquery.sticky.js',array('jquery'),'1.0', true );

	/* OWL CAROUSEL */
	wp_enqueue_script('owl-carousel', get_template_directory_uri().'/js/owl-carousel/owl.carousel.min.js');


	/* WAYPOINT */
	//wp_enqueue_script('waypoint', get_template_directory_uri().'/js/waypoints.js');

	/* COUNTER JS */
	//wp_enqueue_script( 'cowntdown', get_template_directory_uri().'/js/cowntdown/countdown.js',array('jquery'));

	/* PRELOADER */
	//wp_enqueue_script('preloader', get_template_directory_uri().'/js/jquery.queryloader2.min.js', array('jquery'),'1.0', false);

	//wp_enqueue_script('lazy-load', get_template_directory_uri().'/js/jquery.lazyload.min.js' );

	/* FLEXSLIDER */
	wp_enqueue_script('flexslider', get_template_directory_uri().'/js/flexslider/jquery.flexslider.js');
	wp_enqueue_script('shCore', get_template_directory_uri().'/js/flexslider/js/shCore.js');
	wp_enqueue_script('shBrushXml', get_template_directory_uri().'/js/flexslider/js/shBrushXml.js');
	wp_enqueue_script('shBrushJScript', get_template_directory_uri().'/js/flexslider/js/shBrushJScript.js');
	wp_enqueue_script('mousewheel', get_template_directory_uri().'/js/flexslider/js/jquery.easing.js');
	wp_enqueue_script('easing', get_template_directory_uri().'/js/flexslider/js/jquery.mousewheel.js');

	/* NEWS TICKER */

	//wp_enqueue_script('newsticker', get_template_directory_uri() . '/js/newsticker/includes/jquery.ticker.js');

	/* FRESCO LIGHTBOX */
	//wp_enqueue_script('fresco', get_template_directory_uri() . '/js/fresco/fresco.js');

	/* Transit */
	//wp_enqueue_script('transit', get_template_directory_uri() . '/js/jquery.transit.js');


	/* TOOLTIPSTER */
	//wp_enqueue_script('tooltipster', get_template_directory_uri() . '/js/tooltipster/js/jquery.tooltipster.min.js');

	//wp_enqueue_script('instafeed', get_template_directory_uri() . '/js/instafeed.min.js');

	wp_enqueue_script('custom', get_template_directory_uri().'/js/custom.js', array('jquery'),'1.0', true );
}
add_action('wp_enqueue_scripts', 'bootstrapwp_js_loader');




/**
 * upbootwp_less function.
 * Load less for development or even on the running website. If you want to use less just enable this function
 * @access public
 * @return void
 */
function upbootwp_less() {
	printf('<link rel="stylesheet" type="text/less" href="%s" />', get_template_directory_uri().'/less/bootstrap.less?ver=0.1'); // raus machen :)
	printf('<script type="text/javascript" src="%s"></script>', get_template_directory_uri().'/js/less.js');
}
// Enable this when you want to work with less
//add_action('wp_head', 'upbootwp_less');



/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
//require get_template_directory().'/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
//require get_template_directory().'/inc/extras.php';

/**
 * Customizer additions.
 */
//require get_template_directory().'/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
//require get_template_directory().'/inc/jetpack.php';


/**
 * upbootwp_breadcrumbs function.
 * Edit the standart breadcrumbs to fit the bootstrap style without producing more css
 * @access public
 * @return void
 */
function upbootwp_breadcrumbs() {

	$delimiter = '&raquo;';
	$home = 'Home';
	$before = '<li class="active">';
	$after = '</li>';

	if (!is_home() && !is_front_page() || is_paged()) {

		echo '<ol class="breadcrumb">';

		global $post;
		$homeLink = get_bloginfo('url');
		echo '<li><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . '</li> ';

		if (is_category()) {
			global $wp_query;
			$cat_obj = $wp_query->get_queried_object();
			$thisCat = $cat_obj->term_id;
			$thisCat = get_category($thisCat);
			$parentCat = get_category($thisCat->parent);
			if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
			echo $before . single_cat_title('', false) . $after;

		} elseif (is_day()) {
			echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li> ' . $delimiter . ' ';
			echo '<li><a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a></li> ' . $delimiter . ' ';
			echo $before . get_the_time('d') . $after;

		} elseif (is_month()) {
			echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li> ' . $delimiter . ' ';
			echo $before . get_the_time('F') . $after;

		} elseif (is_year()) {
			echo $before . get_the_time('Y') . $after;

		} elseif (is_single() && !is_attachment()) {
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a></li> ' . $delimiter . ' ';
				echo $before . get_the_title() . $after;
			} else {
				$cat = get_the_category(); $cat = $cat[0];
				echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
				echo $before . get_the_title() . $after;
			}

		} elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
			$post_type = get_post_type_object(get_post_type());
			echo $before . $post_type->labels->singular_name . $after;

		} elseif (is_attachment()) {
			$parent = get_post($post->post_parent);
			$cat = get_the_category($parent->ID); $cat = $cat[0];
			echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
			echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a></li> ' . $delimiter . ' ';
			echo $before . get_the_title() . $after;

		} elseif ( is_page() && !$post->post_parent ) {
			echo $before . get_the_title() . $after;

		} elseif ( is_page() && $post->post_parent ) {
			$parent_id  = $post->post_parent;
			$breadcrumbs = array();
			while ($parent_id) {
				$page = get_page($parent_id);
				$breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a></li>';
				$parent_id  = $page->post_parent;
			}
			$breadcrumbs = array_reverse($breadcrumbs);
			foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
			echo $before . get_the_title() . $after;

		} elseif ( is_search() ) {
			echo $before . 'Search results for "' . get_search_query() . '"' . $after;

		} elseif ( is_tag() ) {
			echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;

		} elseif ( is_author() ) {
			global $author;
			$userdata = get_userdata($author);
			echo $before . 'Articles posted by ' . $userdata->display_name . $after;

		} elseif ( is_404() ) {
			echo $before . 'Error 404' . $after;
		}

		if ( get_query_var('paged') ) {
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
			echo ': ' . __('Page') . ' ' . get_query_var('paged');
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
		}

		echo '</ol>';

	}
}





/*
| --------------------------------------------------------------------------------------------------------------------------------------
|
|
|										Qui inizia Dueper
|
|
| --------------------------------------------------------------------------------------------------------------------------------------
|
| */

/*
| -------------------------------------------------------------------
| Rimuovo avviso di upgrade nel backend
| -------------------------------------------------------------------
|
| */

add_action('admin_menu','wphidenag');
function wphidenag() {
remove_action( 'admin_notices', 'update_nag', 3 );
}



/*
| -------------------------------------------------------------------
| Personalizzo URL del Logo nella pagina di login
| -------------------------------------------------------------------
|
| */
add_filter( 'login_headerurl', 'my_custom_login_url' );
function my_custom_login_url($url) {
	$siteURL = get_bloginfo('url');
	return $siteURL;
}



/*
| -------------------------------------------------------------------
| Personalizzo Logo nella pagina di login
| -------------------------------------------------------------------
|
| */

function dueper_custom_loginlogo() {
echo '<style type="text/css">
h1 a {background-image: url('.get_bloginfo('template_directory').'/img/login-logo.png) !important;background-size: inherit !important;height: 63px !important;width: 274px !important;
}

</style>';
}
add_action('login_head', 'dueper_custom_loginlogo');




/*
| -------------------------------------------------------------------
| Personalizzo Logo nel back-end di wordpress
| -------------------------------------------------------------------
|
| */

add_action('admin_head', 'dueper_admin_custom_logo');
add_action('wp_head', 'dueper_admin_custom_logo');

function dueper_admin_custom_logo() {
echo '
<style type="text/css">
#wp-admin-bar-wp-logo { display:none !important; }
wp-admin-bar-site-name > .ab-item:hover { background-color:none !important;}
#wp-admin-bar-site-name.hover > .ab-item { background-image: url('.get_bloginfo('template_directory').'/img/custom-admin-logo-hover.png) !important; background-position:center !important; background-repeat: no-repeat !important;}
</style>
';
}


/*
| -------------------------------------------------------------------
| Personalizzo Footer nel back-end di wordpress
| -------------------------------------------------------------------
|
| */

function remove_footer_admin () {
echo 'Creato da <a href="http://www.dueper.net" target="_blank">Dueper Design snc</a> a partire da <a href="http://www.wordpress.org" target="_blank">WordPress</a></p>';
}

add_filter('admin_footer_text', 'remove_footer_admin');



/*
| -------------------------------------------------------------------
| Disabilito widget nella dashboard di wordpress
| -------------------------------------------------------------------
|
| */

function dueper_remove_dashboard_widgets() {
	global $wp_meta_boxes;

	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // disabilito quick press nella dashboard
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); // disabilito Link in entrata nella dashboard
	//unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // disabilito Riassunto sito (numero di post/commenti ecc..) nella dashboard
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // disabilito widget plugin nella dashboard
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // disabilito widget Bozze recenti nella dashboard
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // disabilito widget dei commenti recenti nella dashboard
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // disabilito widget Wordpress Blog nella dashboard
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); // disabilito widget Other Wordpress news nella dashboard

}

add_action('wp_dashboard_setup', 'dueper_remove_dashboard_widgets' );




/*
| -------------------------------------------------------------------
| Aggiungo widget personalizzato nel back-end di wordpress
| -------------------------------------------------------------------
|
| */

add_action('wp_dashboard_setup', 'dueper_dashboard_widgets');

function dueper_dashboard_widgets() {
global $wp_meta_boxes;

wp_add_dashboard_widget('dueper_help_widget', 'Hai bisogno di aiuto?', 'dueper_dashboard_help');
}

function dueper_dashboard_help() {
echo '<p><h2>Hai riscontrato dei problemi che non rieci a risolvere?</h2><br /><a href="http://www.dueper.net" target="_blank" class="button button-primary button-large">Contattaci</a></p>';
}




add_filter( 'avatar_defaults', 'dueper_newgravatar' );

function dueper_newgravatar ($avatar_defaults) {
$myavatar = get_bloginfo('template_directory') . '/img/gravatar.png'; //inserire nella cartella /img/ la png
$avatar_defaults[$myavatar] = get_current_theme();
return $avatar_defaults;
}



// rimuovo informazioni
add_filter('user_contactmethods','dueper_hide_profile_fields',10,1);

function dueper_hide_profile_fields( $contactmethods ) {
unset($contactmethods['aim']);
unset($contactmethods['jabber']);
unset($contactmethods['yim']);
return $contactmethods;
}

// aggiungo nuove informazioni
function dueper_new_contact_methods( $contactmethods ) {
// Add Twitter
$contactmethods['twitter'] = 'Twitter';
//add Facebook
$contactmethods['facebook'] = 'Facebook';
//add Facebook
$contactmethods['youtube'] = 'Youtube';

return $contactmethods;
}
add_filter('user_contactmethods','dueper_new_contact_methods',10,1);


/*
| -------------------------------------------------------------------
| Aggiungo Immagine copertnina nei feed RSS
| -------------------------------------------------------------------
|
| */

function dueper_rss_post_thumbnail($content) {
global $post;
if(has_post_thumbnail($post->ID)) {
$content = '<p>' . get_the_post_thumbnail($post->ID) .
'</p>' . get_the_content();
}
return $content;
}
add_filter('the_excerpt_rss', 'dueper_rss_post_thumbnail');
add_filter('the_content_feed', 'dueper_rss_post_thumbnail');



/*
| -------------------------------------------------------------------
| Regolo la compressione delle immagini
| (valore fornito nel pannello del tema nel backend)
| -------------------------------------------------------------------
|
| */
function dueper_qualita(){
	$compressione = '100'; // assegno alla compressione il valore settato nel pannello del tema nel backend
	return $compressione;
	}

add_filter('jpeg_quality', 'dueper_qualita' );


/*
| -------------------------------------------------------------------
| Sostituisco Howdy con un testo custom
| (valore fornito nel pannello del tema nel backend)
| -------------------------------------------------------------------
|
| */

add_action( 'admin_bar_menu', 'wp_admin_bar_my_custom_account_menu', 11 );

function wp_admin_bar_my_custom_account_menu( $wp_admin_bar ) {
$user_id = get_current_user_id();
$current_user = wp_get_current_user();
$profile_url = get_edit_profile_url( $user_id );

if ( 0 != $user_id ) {
/* Add the "My Account" menu */
$avatar = get_avatar( $user_id, 28 );
$howdy = sprintf( __('YO! %1$s'), $current_user->display_name );
$class = empty( $avatar ) ? '' : 'with-avatar';

$wp_admin_bar->add_menu( array(
'id' => 'my-account',
'parent' => 'top-secondary',
'title' => $howdy . $avatar,
'href' => $profile_url,
'meta' => array(
'class' => $class,
),
) );

}
}

/*
| -------------------------------------------------------------------
| Imposta il link di default delle immagini a NONE
| -------------------------------------------------------------------
| */
function dueper_imagelink_setup() {
	$image_set = get_option( 'image_default_link_type' );

	if ($image_set !== 'none') {
		update_option('image_default_link_type', 'none');
	}
}
add_action('admin_init', 'dueper_imagelink_setup', 10);


/*
| -------------------------------------------------------------------
| Revising Default Excerpt
| -------------------------------------------------------------------
| Adding filter to post excerpts to contain ...Continue Reading link
| */
function bootstrapwp_excerpt($more) {
  global $post;
  		return '...&nbsp;';
}
add_filter('excerpt_more', 'bootstrapwp_excerpt');


/*
| -------------------------------------------------------------------
| Revising Default Excerpt
| -------------------------------------------------------------------
| Adding filter to post excerpts to contain ...Continue Reading link
| */
function dueper_excerpt_length( $length ) {
	return 21;
}
add_filter( 'excerpt_length', 'dueper_excerpt_length', 999 );



/*
| -------------------------------------------------------------------
| Imposto le option pages
| -------------------------------------------------------------------
| Adding filter to post excerpts to contain ...Continue Reading link
| */

if( function_exists('acf_set_options_page_menu') )
{
    acf_set_options_page_menu( __('Generali') );
}

if( function_exists('acf_add_options_sub_page') )
{
    acf_add_options_sub_page(array(
        'title' => 'General',
        'capability' => 'manage_options'
    ));
    acf_add_options_sub_page(array(
        'title' => 'Social',
        'capability' => 'manage_options'
    ));
}

/*
| -------------------------------------------------------------------
| Aggiungo funzione per is_post_type
| -------------------------------------------------------------------
|
| */

function is_post_type($type){
    global $wp_query;
    if($type == get_post_type($wp_query->post->ID)) return true;
    return false;
}

/*
| -------------------------------------------------------------------
| Custom WOOCOMMERCE FUNCTIONS
| -------------------------------------------------------------------
|
| */

//disabilito le revisioni
add_filter( 'woocommerce_product_tabs', 'disable_woocommerce_reviews', 98);
function disable_woocommerce_reviews($tabs) {
    unset($tabs['reviews']);
    return $tabs;
}

// customizzo separatore breadcrumbs
add_filter( 'woocommerce_breadcrumb_defaults', 'jk_change_breadcrumb_delimiter' );
function jk_change_breadcrumb_delimiter( $defaults ) {
	// Change the breadcrumb delimeter from '/' to '>'
	$defaults['delimiter'] = ' &gt; ';
	return $defaults;
}



/*
| -------------------------------------------------------------------
| Aggiungo elementi menu user
| -------------------------------------------------------------------
|
| */
add_filter( 'wp_nav_menu_items', 'your_custom_menu_item', 10, 2 );
function your_custom_menu_item( $items, $args ) {
    global $woocommerce;
    $cartcount = "";
    if(sizeof( $woocommerce->cart->cart_contents ) > 0){
        $cartcount = $woocommerce->cart->cart_contents_count;
    }

    if ($args->theme_location == 'user') {
        if(is_user_logged_in()){
            $items .= '<li class="menu-item"><a href="'.get_permalink( get_page_by_title( 'My Account' ) ).'">'.__('Profilo').'</a></li>';
            $items .= '<li class="menu-item"><a href="'.wp_logout_url( get_permalink( wc_get_page_id( 'myaccount' ) ) ).'">'.__('Esci').'</a></li>';
            $items .= '<li class="menu-item"><a class="call-searchbar"><span class="glyphicon glyphicon-search"></span></a></li>';
            $items .= '<li class="cart-item menu-item"><a class="cart-count-menu" href="'.$woocommerce->cart->get_cart_url().'"><span class="icon-shopping-cart"></span> '.$cartcount.'</a></li>';
        } else {
            $items .= '<li class="menu-item"><a class="call-searchbar"><span class="glyphicon glyphicon-search"></span></a></li>';
            $items .= '<li class="menu-item"><a href="'.get_permalink( get_page_by_title( 'My Account' ) ).'">'.__('Accedi').'</a></li>';
            $items .= '<li class="cart-item menu-item"><a class="cart-count-menu" href="'.$woocommerce->cart->get_cart_url().'"><span class="icon-shopping-cart"></span> '.$cartcount.'</a></li>';
        };
    }
    if ($args->theme_location == 'mobile') {
        if(is_user_logged_in()){
            $items .= '<li class="menu-item"><a href="'.get_permalink( get_page_by_title( 'My Account' ) ).'">'.__('Profilo').'</a></li>';
            $items .= '<li class="menu-item"><a href="'.wp_logout_url( get_permalink( wc_get_page_id( 'myaccount' ) ) ).'">'.__('Esci').'</a></li>';
            // $items .= '<li class="menu-item"><a class="call-searchbar"><span class="glyphicon glyphicon-search"></span></a></li>';
            $items .= '<li class="cart-item menu-item"><a class="cart-count-menu" href="'.$woocommerce->cart->get_cart_url().'"><span class="icon-shopping-cart"></span> '.$cartcount.'</a></li>';
        } else {
            // $items .= '<li class="menu-item"><a class="call-searchbar"><span class="glyphicon glyphicon-search"></span></a></li>';
            $items .= '<li class="menu-item"><a href="'.get_permalink( get_page_by_title( 'My Account' ) ).'">'.__('Accedi').'</a></li>';
            $items .= '<li class="cart-item menu-item"><a class="cart-count-menu" href="'.$woocommerce->cart->get_cart_url().'"><span class="icon-shopping-cart"></span> '.$cartcount.'</a></li>';
        };
    }
    return $items;
}

/*
| -------------------------------------------------------------------
| Aggiorno carrello ajax
| -------------------------------------------------------------------
|
| */
add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;
        ob_start();
        echo '<a class="cart-count-menu" href="'.$woocommerce->cart->get_cart_url().'"><span class="icon-shopping-cart"></span> '.$woocommerce->cart->cart_contents_count.'</a>';
        $fragments['a.cart-count-menu'] = ob_get_clean();
	return $fragments;

}


/**
 * WooCommerce Extra Feature
 * --------------------------
 *
 * Change number of related products on product page
 * Set your own value for 'posts_per_page'
 *
 */
function woo_related_products_limit() {
  global $product;

	$args['posts_per_page'] = 6;
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'dueper_related_products_args' );
  function dueper_related_products_args( $args ) {

	$args['posts_per_page'] = 4; // 4 related products
	$args['columns'] = 4; // arranged in 2 columns
	return $args;
}



/*
| -------------------------------------------------------------------
| SOCIAL META
| -------------------------------------------------------------------
|
| */
function get_social_meta() {
    $output = "";
    $default_image = get_field('immagine_di_default','option');

    if(is_front_page()) :

        $output .= '<meta property="og:title" content="'.get_bloginfo('name').'" />';
        $output .= '<meta property="og:site_name" content="'.get_bloginfo('name').'" />';
        $output .= '<meta property="og:url" content="'.esc_url( home_url( '/' ) ).'" />';
        $output .= '<meta property="og:type" content="website" />';
        $output .= '<meta property="og:description" content="'.get_field('slogan_home','option').'" />';
        $output .= '<meta property="og:image" content="'. $default_image['sizes']['large'].'" />';

    else :

        $image_preview = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
        if($image_preview) :
            $image_URL = $image_preview[0];
        else:
            $image_URL = $default_image['sizes']['large'];
        endif;


        $output .= '<meta property="og:title" content="'.get_the_title().'" />';
        $output .= '<meta property="og:site_name" content="'.get_bloginfo('name').'" />';
        $output .= '<meta property="og:url" content="'.get_permalink().'" />';
        $output .= '<meta property="og:type" content="article" />';
        $output .= '<meta property="og:image" content="'.$image_URL.'" />';

    endif;

    return $output;
}


/* ADD "woocmmerce" BODY CLASS to singole-produttore.php */
function add_body_class_to_produttore($classes){
  if(is_singular('produttore')):
    $classes[] = 'woocommerce';
  endif;
  return $classes;
}
// Apply filter
add_filter('body_class', 'add_body_class_to_produttore');



// Custom Credit Card Icons
// add_filter ('woocommerce_gateway_icon', 'custom_woocommerce_icons');
//
// function custom_woocommerce_icons($icon) {
// 		var_dump($icon);
// 	if($gateway_id == 'paypal'){
//     $icon  = '<img src="'.get_template_directory_uri().'/img/gravatar.png" />';
// 	}
//     return $icon;
// }

function custom_wc_gateway_icons( $icon, $gateway_id ) {
	// Example for PayPal:
	if ( 'paypal' == $gateway_id ) {
		$icon = '<img src="'.get_template_directory_uri().'/img/credit-cards.png" />';
	}
	return $icon;
}
add_filter( 'woocommerce_gateway_icon', 'custom_wc_gateway_icons', 10, 2 );
