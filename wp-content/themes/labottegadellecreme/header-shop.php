<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <main id="main">
 *
 * @author Dueper Design
 * @package Dueper Theme
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title><?php wp_title( '|', true, 'right' ); bloginfo('name'); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>

<?php echo get_social_meta(); ?>

<?php wp_head(); ?>
<!--[if lt IE 9]>
    <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script type='text/javascript' src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
<![endif]-->


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59967742-1', 'auto');
  ga('send', 'pageview');

</script>
  

</head>

<body <?php body_class(); ?>>
<div id="main-wrapper">
    <div class="menu-container" id="sticker">
        <!--container-->
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="col-sm-4 menu-left">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="navbar-header">
                            
                            <?php 
                            $args = array('theme_location' => 'primary', 
                                          'container_class' => 'navbar-collapse collapse pull-left', 
                                          'menu_class' => 'nav navbar-nav',
                                          'fallback_cb' => '',
                                          'menu_id' => 'main-menu',
                                          'walker' => new Upbootwp_Walker_Nav_Menu()); 
                            wp_nav_menu($args);
                            ?>

                        </div>
                    </nav>
                </div><!-- .col-md-9 -->
                <div class="col-sm-12 col-md-4 brand-container">
                    <a class="navbar-brand" href="/">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-bottega-w.png" alt="<?php bloginfo('name'); ?>" class="logo-white img-responsive" />
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-bottega-b.png" alt="<?php bloginfo('name'); ?>" class="logo-black img-responsive" />
                    </a>	
                </div><!-- .col-md-4 -->
                <div class="col-sm-4 menu-right">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="navbar-header">
                            
                            <button type="button" id="menu-toggle" class="navbar-toggle">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            
                            <?php 
                            $args = array('theme_location' => 'user', 
                                          'container_class' => 'navbar-collapse collapse pull-right', 
                                          'menu_class' => 'nav navbar-nav',
                                          'fallback_cb' => '',
                                          'menu_id' => 'main-menu',
                                          'walker' => new Upbootwp_Walker_Nav_Menu()); 
                            wp_nav_menu($args);
                            ?>

                        </div>
                    </nav>
                </div><!-- .col-md-9 -->
            </div><!-- row -->                        
        </div><!-- container -->
	</div>
    
	<div id="site-content">
    

    <?php
    $slide_args = array(
      'post_type'=>'slides',
      'posts_per_page'=> 1
    );
    $slider = new WP_Query($slide_args);
    if($slider->have_posts()): ?>
        <div class="intro-slider">
            <div class="frame-top"></div>
            <div class="frame-right"></div>
            <div class="frame-bottom"></div>
            <div class="frame-left"></div>

            <?php while($slider->have_posts()): $slider->the_post();
                $image = get_field('immagine_slide');?>
                <div class="bg-header" style="background-image:url('<?php echo $image['sizes']['bg-fascia'] ?>');" data-adaptive-background='1' data-ab-css-background='1'>
                    <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
                        <h1 class="page-title"><?php woocommerce_page_title(); ?></h1>
                    <?php endif; ?>
                </div><!--item-->
            <?php endwhile; ?>
        </div><!--intro-head-->
    <?php endif; wp_reset_query(); ?>