<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @author Dueper Design
 * @package Dueper Theme
 */

global $query_string;
query_posts( $query_string . '&posts_per_page=5' );

get_header(); ?>
	<div class="container">
    	<div class="row">
        	<div class="col-md-8">
				<?php if ( have_posts() ) :
                    while ( have_posts() ) : the_post(); ?>             
                    	<article <?php post_class(); ?>>
                        	<date><?php echo get_the_date(); ?></date>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                            	<h1 class="single-title"><?php the_title(); ?></h1>
                            </a>
                            <?php the_post_thumbnail('thumbnail',array('class'=>'img-responsive')); ?>
                            <?php the_excerpt(); ?>
                        </article><!--post_class-->
                    <?php endwhile; ?>
                    
                <?php endif; ?>
        	</div><!--col-md-8-->
            <div class="col-md-4">
            	<?php get_sidebar(); ?>
            </div><!--col-md-4-->
		</div><!--row-->
	</div><!--container-->

<?php get_footer(); ?>
