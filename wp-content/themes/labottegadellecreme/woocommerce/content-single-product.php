<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

    <?php
        /**
         * woocommerce_before_single_product hook
         *
         * @hooked wc_print_notices - 10
         */
         do_action( 'woocommerce_before_single_product' );

         if ( post_password_required() ) {
            echo get_the_password_form();
            return;
         }
    ?>


<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

        <?php
            /**
             * woocommerce_before_single_product_summary hook
             *
             * @hooked woocommerce_show_product_sale_flash - 10
             * @hooked woocommerce_show_product_images - 20
             */
            do_action( 'woocommerce_before_single_product_summary' );
        ?>
    
        <div class="summary entry-summary">

            <?php
                /**
                 * woocommerce_single_product_summary hook
                 *
                 * @hooked woocommerce_template_single_title - 5
                 * @hooked woocommerce_template_single_rating - 10
                 * @hooked woocommerce_template_single_price - 10
                 * @hooked woocommerce_template_single_excerpt - 20
                 * @hooked woocommerce_template_single_add_to_cart - 30
                 * @hooked woocommerce_template_single_meta - 40
                 * @hooked woocommerce_template_single_sharing - 50
                 */
                //do_action( 'woocommerce_single_product_summary' );

                woocommerce_template_single_title();
                woocommerce_template_single_price();
                woocommerce_template_single_add_to_cart();
                the_content();
                woocommerce_template_single_meta();
            ?>
            
            <nav class="post-navigation">
                <?php previous_post_link('%link','<span class="icon-wrap"><span class="icon-chevron-left"></span></span><h3>%title</h3>'); ?>
                <?php next_post_link('%link','<span class="icon-wrap"><span class="icon-chevron-right"></span></span><h3>%title</h3>'); ?>
            </nav>
            
        </div><!-- .summary -->
        <div class="clear"></div>
    <?php get_template_part('includes/prodotto-tab'); ?>
    
	<?php
		/**
		 * woocommerce_after_single_product_summary hook
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		//do_action( 'woocommerce_after_single_product_summary' );
	?>

	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->


<?php
global $product;
$upsells = $product->get_upsells();
if ( sizeof( $upsells ) > 0 ):
    woocommerce_upsell_display();
else:
    woocommerce_output_related_products();    
endif; ?>
    
<?php do_action( 'woocommerce_after_single_product' ); ?>
