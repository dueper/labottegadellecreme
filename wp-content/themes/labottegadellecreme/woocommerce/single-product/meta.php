<?php
/**
 * Single Product Meta
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post, $product;

$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );
?>
<div class="product_meta">
	<?php do_action( 'woocommerce_product_meta_start' ); ?>
    
	<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

		<span class="sku_wrapper"><?php _e( 'SKU:', 'woocommerce' ); ?> <span class="sku" itemprop="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : __( 'N/A', 'woocommerce' ); ?></span>.</span>

	<?php endif; ?>

	<?php echo $product->get_categories( ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', $cat_count, 'woocommerce' ) . ' ', '.</span>' ); ?>

	<?php echo $product->get_tags( ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', $tag_count, 'woocommerce' ) . ' ', '.</span>' ); ?>
    
    <?php
    $produttore = get_field('produttore');
    if($produttore):
      if(is_object($produttore[0])):
        //var_dump($produttore);
        echo __( 'Produttore', 'woocommerce' ).' <a href="#modal-'.$produttore[0]->ID.'" data-toggle="modal" data-target="#modal-'. $produttore[0]->ID .'" title="'.$produttore[0]->post_title.'">'.$produttore[0]->post_title.'</a>';
        ?>
        <!-- Modal -->
        <div class="modal modal-produttore fade" id="modal-<?php echo $produttore[0]->ID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

              <div class="modal-body">
                <figure><?php echo get_the_post_thumbnail($produttore[0]->ID,'medium',array('class'=>'img-responsive aligncenter')); ?></figure>
                <h4 class="modal-title" id="myModalLabel"><?php echo $produttore[0]->post_title; ?></h4>
                <?php echo $produttore[0]->post_content; ?>
              </div>
              <div class="modal-footer">
                <?php $link = get_field('sito_web', $produttore[0]->ID);
                if($link): 
                    echo '<a href="'.$link['url'].'" target="_blank" class="btn btn-close">'. __('Vai al sito').'</a>';
                endif; ?>
                <a href="<?php echo get_permalink($produttore[0]->ID); ?>" class="btn btn-link"><?php _e('Vedi tutti i prodotti'); ?></a>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal -->
      <?php else : // if $produttore[0] is object
      echo __( 'Produttore', 'woocommerce' ).' <a href="#modal-'.$produttore[0].'" data-toggle="modal" data-target="#modal-'. $produttore[0] .'" title="'. get_the_title($produttore[0]) .'">'.get_the_title($produttore[0]).'</a>';
        ?>
        <!-- Modal -->
        <div class="modal modal-produttore fade" id="modal-<?php echo $produttore[0]; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

              <div class="modal-body">
                <figure><?php echo get_the_post_thumbnail($produttore[0],'medium',array('class'=>'img-responsive aligncenter')); ?></figure>
                <h4 class="modal-title" id="myModalLabel"><?php echo get_the_title($produttore[0]);  ?></h4>
                <?php echo get_the_content($produttore[0]); ?>
              </div>
              <div class="modal-footer">
                <?php $link = get_field('sito_web', $produttore[0]);
                if($link): 
                    echo '<a href="'.$link['url'].'" target="_blank" class="btn btn-close">'. __('Vai al sito').'</a>';
                endif; ?>
                <a href="<?php echo get_permalink($produttore[0]); ?>" class="btn btn-link"><?php _e('Vedi tutti i prodotti'); ?></a>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal -->
        
      <?php endif; // if $produttore[0] is object ?>
    <?php endif; ?>
    
	<?php do_action( 'woocommerce_product_meta_end' ); ?>
    

</div>