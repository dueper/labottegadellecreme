<?php
/**
 * The template for displaying Search Results pages.
 *
 * @author Dueper Design
 * @package Dueper Theme
 */

get_header(); ?>
			
<?php if ( have_posts() ) : ?>
	<div class="container">
    	<h1><?php printf( __( 'Search Results for: %s', 'upbootwp' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
    	<div class="row">
        	<div class="col-md-8">
				<?php while ( have_posts() ) : the_post(); ?>
                    <article <?php post_class(); ?>>
                        <date><?php echo get_the_date(); ?></date>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                            <h1 class="single-title"><?php the_title(); ?></h1>
                        </a>
                        <?php the_post_thumbnail('thumbnail',array('class'=>'img-responsive')); ?>
                        <?php the_excerpt(); ?>
                    </article><!--post_class-->
                <?php endwhile; ?>
            </div><!--col-md-8-->
            <div class="col-md-4">
            	<?php get_sidebar(); ?>
            </div>
        </div><!--row-->
    </div><!--container-->
    
<?php else : ?>

	<div class="container">
	    <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <strong class="top-spaced"><?php _e( 'Nothing Found', 'upbootwp' ); ?></strong>
            </div><!--col-md-10-->
        </div><!-- row -->
	    <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'upbootwp' ); ?></p>
                <?php get_search_form(); ?>
            </div><!--col-md-10-->
        </div><!-- row -->
	</div><!-- container -->
	
<?php endif; ?>
			
<?php get_footer(); ?>