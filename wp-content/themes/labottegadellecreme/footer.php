<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @author Dueper Design
 * @package Dueper Theme1
 */

get_template_part('includes/fascia-social');
 
?>

    </div><!--#site-content-->
    <footer class="site-foot">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-push-6">
                    <?php 
                    $args_footer = array(
                            'theme_location' => 'footer',
                            'menu_id'         => 'footer-menu',
                        ); 
                    wp_nav_menu($args_footer);
                    ?>
                </div><!--col-md-5-->
                <div class="col-md-6  col-md-pull-6">
                    <div class="copyright"><?php the_field('credits_footer','option');  ?><br />Created by <a href="http://www.dueper.net" target="_blank">Dueper Design</a></div>
                </div><!--col-md-5-->
            </div><!--row-->
            
        </div><!--container-->
    </footer>

</div><!--#main-wrapper-->

<div class="search-form-hidden">
    <?php get_product_search_form(); ?>
    
</div>    

<?php 
$args1 = array('theme_location' => 'mobile',
			  'container_id' => 'mobile-menu', 
			  'fallback_cb' => '',
			  'menu_id' => 'mobile-menu',
			  'walker' => new Mobile_Walker_Nav_Menu()
		); 
wp_nav_menu($args1);
?>

<?php wp_footer(); ?>
</body>
</html>