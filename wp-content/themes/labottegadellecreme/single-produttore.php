<?php
/**
 * The Template for displaying all single posts.
 *
 * @author Dueper Design
 * @package Dueper Theme
 */
get_header(); the_post();
  $produttore = get_the_title(); ?>
  <div class="container">
      <div class="row">
        <div class="col-md-12 col-lg-10 col-lg-offset-1 page-content">
          <div class="row">
              <div class="col-md-8 col-md-offset-2 text-center">
                  <article <?php post_class('produttore-content'); ?>>
                      <figure>
                        <?php the_post_thumbnail('thumbnail',array('class'=>'img-responsive')); ?>
                      </figure>
                      <div class="text-justify">
                        <?php the_content(); ?>
                      </div>
                      <footer class="text-right">
                        <?php $link = get_field('sito_web');
                        if($link): 
                            echo '<a href="'.$link['url'].'" target="_blank" class="btn btn-website">'. __('Vai al sito').'</a>';
                        endif; ?>
                      </footer>
                  </article><!--post-class-->

              </div><!--col-md-4-->
          </div><!--row produttori-content-->
        </div><!--col-md-10-->
      </div><!--row-->
  </div><!--container-->
  
  
  <?php
  $current_id = $post->ID;
  $args = array(
    'post_type'     => 'product',
    'posts_per_page'=> -1,
    'post_status'   => 'any',
    'meta_query'    => array(
                          array(
                            'key'   => 'produttore',
                            'value' => '"' . $current_id . '"',
                            'compare' => 'LIKE'
                          ),
                        ),
  );
  $prducts = new WP_Query($args);
  if ( $prducts->have_posts() ) : ?>
    <div class="container">
      <h4 class="list-title"><?php echo __('I prodotti di').' <span>'.$produttore.'</span>'; ?></h4>
      <?php woocommerce_product_loop_start(); ?>
          <?php while ( $prducts->have_posts() ) : $prducts->the_post(); ?>
              <?php wc_get_template_part( 'content', 'product' ); ?>
          <?php endwhile; // end of the loop. ?>
      <?php woocommerce_product_loop_end(); ?>
    </div>
  <?php endif; ?>

<?php
get_footer(); ?>