<?php
/**
 * The template for displaying search forms in upBootWP
 *
 * @author Dueper Design
 * @package Dueper Theme
 */
?>
<form role="search" method="get" class="search-form form-inline" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="form-group">
		<input type="search" class="search-field form-control" placeholder="<?php echo esc_attr_x( 'CERCA', 'placeholder', 'upbootwp' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" title="<?php _ex( 'Search for:', 'label', 'upbootwp' ); ?>">
	</div>
    <button type="submit" class="search-submit" ><span class="glyphicon glyphicon-search"></span></button>
    <input type="hidden" name="post_type" value="product">
</form>
