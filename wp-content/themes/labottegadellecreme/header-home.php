<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <main id="main">
 *
 * @author Dueper Design
 * @package Dueper Theme
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta name="google-site-verification" content="EyfiwFUAM1iKwkuY3kEo4WdDv8LyZzY2SwjYaV1d01k" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title><?php wp_title( '|', true, 'right' ); bloginfo('name'); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>

<?php echo get_social_meta(); ?>

<?php wp_head(); ?>
<!--[if lt IE 9]>
    <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script type='text/javascript' src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
<![endif]-->


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59967742-1', 'auto');
  ga('send', 'pageview');

</script>
  <script type="text/javascript">
	var _iub = _iub || [];
	_iub.csConfiguration = {
		cookiePolicyId: 164356,
		siteId: 216656,
		lang: "it"
	};
	(function (w, d) {
		var loader = function () { var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src = "//cdn.iubenda.com/cookie_solution/iubenda_cs.js"; tag.parentNode.insertBefore(s, tag); };
		if (w.addEventListener) { w.addEventListener("load", loader, false); } else if (w.attachEvent) { w.attachEvent("onload", loader); } else { w.onload = loader; }
	})(window, document);
</script>
</head>

<body <?php body_class(); ?>>


<div id="main-wrapper">
    <div class="menu-container" id="sticker">
        <!--container-->
        <div class="container-fluid">
                <div class="menu-left">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="navbar-header">
                            
                            <?php 
                            $args = array('theme_location' => 'primary', 
                                          'container_class' => 'navbar-collapse collapse pull-left', 
                                          'menu_class' => 'nav navbar-nav',
                                          'fallback_cb' => '',
                                          'menu_id' => 'main-menu',
                                          'walker' => new Upbootwp_Walker_Nav_Menu()); 
                            wp_nav_menu($args);
                            ?>

                        </div>
                    </nav>
                </div><!-- menu-left -->
                <div class="brand-container">
                    <a class="navbar-brand" href="/">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-bottega-w.png" alt="<?php bloginfo('name'); ?>" class="logo-white img-responsive" />
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-bottega-b.png" alt="<?php bloginfo('name'); ?>" class="logo-black img-responsive" />
                    </a>	
                </div><!-- brand-container -->
                <div class="menu-right">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="navbar-header">
                            
                            <button type="button" id="menu-toggle" class="navbar-toggle">
                                 <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            
                            
                            <?php 
                            $args = array('theme_location' => 'user', 
                                          'container_class' => 'navbar-collapse collapse pull-right', 
                                          'menu_class' => 'nav navbar-nav',
                                          'fallback_cb' => '',
                                          'menu_id' => 'user-menu',
                                          'walker' => new Upbootwp_Walker_Nav_Menu()); 
                            wp_nav_menu($args);
                            ?>

                        </div>
                    </nav>
                </div><!--menu-right -->                      
        </div><!-- container -->
	</div>
    
	<div id="site-content">
        
    <?php get_template_part('includes/slider-home'); ?>