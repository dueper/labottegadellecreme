<?php
/**
 * Template Name: Recensioni
 * The template used for displaying page content in page.php
 *
 * @author Dueper Design
 * @package Dueper Theme
 */
get_header();
if(have_posts()): the_post();?>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-10 col-lg-offset-1 page-content recensioni">
            
            <?php
            $recensioni_args = array(
                            'post_type'     => 'messaggi-dicono',
                            'posts_per_page'=> -1
                            );
            $recensioni = new WP_Query($recensioni_args);
            if($recensioni->have_posts()):?>
                    <?php while($recensioni->have_posts()): $recensioni->the_post();
                        $name = get_field('nome_dicono');
                        $link = get_field('link_dicono');
                        ?>
                        <article <?php post_class(); ?>>
                          <header>
                            <h1><?php the_title(); ?></h1>
                          </header>
                          <?php the_content(); ?>
                          <footer>
                            <div class="col-md-6 col-lg-6 text-left">
                            <?php 
                              if($link) echo '<a href="'.esc_url($link).'" title="'.$link.'" target="_blank">';
                              if($link) echo '<span class="link">Leggi qui la recensione completa</span>';
                              if($link) echo '</a>'; ?>
                            </div>
                            <div class="col-md-6 col-lg-6 text-right">
                              <?php echo '<span class="name">'.$name.'</span>';?>
                            </div>
                          </footer>
                        </article><!--col-md-4-->
                    <?php endwhile; ?>
                </div><!--row produttori-content-->
            <?php endif; wp_reset_query(); ?>
            
            <?php the_field('testo_sotto'); ?>     
        </div><!--col-md-10-->
    </div><!--row-->
    <div class="row">
      <div class="col-md-12 col-lg-10 col-lg-offset-1 page-content">
          <?php the_content(); ?>
      </div>
    </div><!--row-->
</div><!--container-->

<?php endif;
get_footer(); ?>