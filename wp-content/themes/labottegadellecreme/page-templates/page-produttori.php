<?php
/**
 * Template Name: Produttori
 * The template used for displaying page content in page.php
 *
 * @author Dueper Design
 * @package Dueper Theme
 */
get_header();
if(have_posts()): the_post();?>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-10 col-lg-offset-1 page-content">
            
            <?php the_field('testo_sopra'); ?>
            
            <?php
            $produttori_args = array(
                            'post_type'     => 'produttore',
                            'posts_per_page'=> -1
                            );
            $produttori = new WP_Query($produttori_args);
            if($produttori->have_posts()):
            ?>
                <div class="row produttori-content">
                    <?php while($produttori->have_posts()): $produttori->the_post(); ?>
                        <div class="col-md-4 col-sm-6">
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                <div <?php post_class(); ?>>
                                    <figure>
                                        <?php the_post_thumbnail('thumbnail',array('class'=>'img-responsive')); ?>
                                    </figure>
                                    <h1>VEDI TUTTI I PRODOTTI</h1>
                                </div><!--post-class-->
                            </a>
                            
                            <?php get_template_part('includes/produttore-modal'); ?>

                        </div><!--col-md-4-->
                    <?php endwhile; ?>
                </div><!--row produttori-content-->
            <?php endif; wp_reset_query(); ?>
            
            <?php the_field('testo_sotto'); ?>
            
        </div><!--col-md-10-->
    </div><!--row-->
</div><!--container-->

<?php endif;
get_footer(); ?>