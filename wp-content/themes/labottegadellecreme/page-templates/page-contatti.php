<?php
/**
 * Template Name: Contatti
 * The template used for displaying page content in page.php
 *
 * @author Dueper Design
 * @package Dueper Theme
 */
get_header();
if(have_posts()): the_post();?>
<div class="section-title bg-green"><span><?php the_title(); ?></span></div>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 page-content">
            <?php the_content(); ?>
        </div><!--col-md-10-->
    </div>
</div><!--container-->

<?php endif;
get_footer(); ?>