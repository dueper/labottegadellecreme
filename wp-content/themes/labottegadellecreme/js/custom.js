// init sul ready
jQuery(document).ready(function() {
    jQuery('.produttori-content .hentry').matchHeight();
});

jQuery(document).ready(function () {
    
    // Inizializzo il bg detect
    var defaults      = {
      selector:             '[data-adaptive-background="1"]',
      parent:               '.menu-container',
      lumaClasses:  {
        light:      "ab-light",
        dark:       "ab-dark"
      }
    };
    jQuery.adaptiveBackground.run(defaults)
    
    // setto l'altezza dello slider
	var totalheight = jQuery(window).height();
	jQuery('.home .intro-slider').height(totalheight);
	
    // Sull'hover dell'head aggiungo la classe .hover e la tolgo all'uscita
    jQuery( '.menu-container' ).hover( function() {
        jQuery( this ).addClass( "hover" );
    }, function() {
        jQuery(this).removeClass( "hover");
    });
    
    // setto il margine del menu
    var headheight = jQuery( '.menu-container' ).height()/2;
    jQuery('.navbar-default').css('margin-top',headheight-25 );
    
    // dropdown toggle
	jQuery('.dropdown-toggle').dropdownHover();
	
    
    ////////////////////////////////////////////////////////////////// Main menu
	jQuery("#mobile-menu").mmenu({
             offCanvas: {
                           position  : "right"
                        }
        }, {
            // configuration
            offCanvas: {
                            label: "label",
                            pageNodetype: "#main-wrapper"
                        }
	});
	jQuery("#menu-toggle").click(function() {
		jQuery("#mobile-menu").trigger("open.mm");
	});
    
    /*
    ////////////////////////////////////////////////////////////////// user mobile menu
    jQuery("#user-mobile-menu").mmenu({
		 offCanvas: {
					   position  : "right"
					}
	}, {
		// configuration
		offCanvas: {
			label: "label",
			pageNodetype: "#main-wrapper"
		}
	});
	jQuery("#user-menu-toggle").click(function() {
		jQuery("#user-mobile-menu").trigger("open.mm");
	});
    */
    
});

// init sul resize
jQuery( window ).resize(function() {
    // setto l'altezza dello slider
	var totalheight = jQuery(window).height();
	jQuery('.home .intro-slider').height(totalheight);
});

function navcolor() {
    setTimeout(function(){
        var slide = jQuery('.flex-active-slide');
        var nav = jQuery('.menu-container');

        if(slide.hasClass('ab-light')) {
            nav.removeClass('darkbg');
            nav.addClass('lightbg');
        } else if(slide.hasClass('ab-dark')) {
            nav.removeClass('lightbg');
            nav.addClass('darkbg');
        };
    }, 10);
};

/* FLEXSLIDER */
jQuery(window).load(function() {
    jQuery('.spinner').fadeOut(500);
    
    jQuery('.intro-slider').flexslider({
        animation: "fade",
        controlNav: false,
        directionNav: false,
        controlNav: true,
        slideshowSpeed: 5000,
        animationSpeed: 1500,
        start: navcolor,
        before: navcolor,
    });
});
jQuery(window).ready(function() {
    var opts = {
        lines: 20, // The number of lines to draw
        length: 10, // The length of each line
        width: 2, // The line thickness
        radius: 30, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#553416', // #rgb or #rrggbb or array of colors
        speed: 1, // Rounds per second
        trail: 81, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        zIndex: 0, // The z-index (defaults to 2000000000)
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        top: '50%', // Top position relative to parent
        left: '50%' // Left position relative to parent
    };
    jQuery('.intro-slider').prepend(new Spinner(opts).spin().el);
});


/********************************************************** Owl Slider */

jQuery.fn.scrollBottom = function() { 
  return jQuery(document).height() - this.scrollTop() - this.height(); 
}; 

jQuery(window).scroll(function(){
    var scroll_bottom = jQuery(this).scrollBottom();
    var scroll_top = jQuery(this).scrollTop()
    
      if (scroll_bottom < 300) {
          
      } else if (scroll_top > 125 && scroll_bottom >= 301) {
          jQuery('.riepilogo-ordine').css("margin-top",scroll_top-125);
      
      } else if( scroll_top < 125 ) {
          jQuery('.riepilogo-ordine').css("margin-top",0);
      }
});

jQuery(document).ready(function() {
    
    jQuery('.sharing').sharrre({
      share: {
        twitter: true,
        facebook: true,
        googlePlus: true
      },
      template: '<div class="sharing-box"><a href="#" class="facebook"><span class="icon-facebook2"></span></a><a href="#" class="twitter"><span class="icon-twitter2"></span></a><a href="#" class="googleplus"><span class="icon-googleplus2"></span></a></div>',
      enableHover: false,
      enableTracking: true,
      render: function(api, options){
      jQuery(api.element).on('click', '.twitter', function() {
        api.openPopup('twitter');
      });
      jQuery(api.element).on('click', '.facebook', function() {
        api.openPopup('facebook');
      });
      jQuery(api.element).on('click', '.googleplus', function() {
        api.openPopup('googlePlus');
      });
    }
    });

  jQuery('.prodotti-carousel').owlCarousel({
		loop:true,
		margin:40,
		dots:true,
		nav:false,
		responsiveClass:true,
		autoplay:true,
		autoplayTimeout:3000,
		responsive:{
			0:{
				items:2,
			},
			600:{
				items:4,
			},
			900:{
				items:8,
			}
		}
	})
  
  /* Search bar switcher */
    
    jQuery('.search-form-hidden #searchform').append('<a id="close-search"><span class="icon-times"></span></a>');
  
    jQuery( "a.call-searchbar" ).click(function() {
        jQuery( ".search-form-hidden" ).addClass('visible')
    });
    jQuery(document).keyup(function(e) {
        if (e.keyCode == 27) {
            jQuery( ".search-form-hidden" ).removeClass('visible');
        }
    });    
    jQuery( "#close-search" ).click(function() {
        jQuery( ".search-form-hidden" ).removeClass('visible');
    });
    
    
    
    
    
    jQuery(document).ready(function(){
        jQuery("#sticker").sticky({topSpacing:0});
    });
  
  jQuery( ".products .type-product" ).hover(
      function() {
        jQuery( this ).addClass( "hover" );
      }, function() {
        jQuery( this ).removeClass( "hover" );
      }
    );
    
});