<?php
/**
 * The Template for displaying all single posts.
 *
 * @author Dueper Design
 * @package Dueper Theme
 */
get_header(); ?>
	<div class="container">
    	<div class="row">
        	<div class="col-md-8">
				<?php if ( have_posts() ) : the_post(); ?>
                	<article <?php post_class(); ?>>
                    	<date><?php echo get_the_date(); ?></date>
                        <h1 class="single-title"><?php the_title(); ?></h1>
                        <?php the_post_thumbnail('thumbnail',array('class'=>'img-responsive alignleft')); ?>
                        <?php the_content(); ?>
                    </article><!--post_class-->
                <?php endif; ?>
			</div><!--col-md-8-->
            <div class="col-md-4">
            	<?php get_sidebar(); ?>
            </div><!--col-md-4-->
		</div><!--row-->
	</div>
<?php get_footer('promo'); ?>
