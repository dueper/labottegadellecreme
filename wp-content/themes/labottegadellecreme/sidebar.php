<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @author Dueper Design
 * @package Dueper Theme
 */
?>
	<div id="secondary" class="widget-area" role="complementary">
		<?php do_action( 'before_sidebar' ); ?>
		<?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) ?>
	</div><!-- #secondary -->
