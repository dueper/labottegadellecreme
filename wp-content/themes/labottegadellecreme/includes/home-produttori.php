<?php
if(have_posts()): the_post();?>
<div class="container">
  <h2 class="text-center">
        Aziende trattate
    </h2>
    <div class="row">
        <div class="col-md-12 col-lg-12 page-content">
            
            <?php
            $produttori_args = array(
                            'post_type'     => 'produttore',
                            'posts_per_page'=> -1
                            );
            $produttori = new WP_Query($produttori_args);
            if($produttori->have_posts()):
            ?>
                <div class="row produttori-content">
                    <?php while($produttori->have_posts()): $produttori->the_post(); ?>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                <div <?php post_class(); ?>>
                                    <figure>
                                        <?php the_post_thumbnail('thumbnail',array('class'=>'img-responsive')); ?>
                                    </figure>
                                    <h1>VEDI TUTTI I PRODOTTI</h1>
                                </div><!--post-class-->
                            </a>
                        </div><!--col-md-4-->
                    <?php endwhile; ?>
                </div><!--row produttori-content-->
            <?php endif; wp_reset_query(); ?>
            
        </div><!--col-md-10-->
    </div><!--row-->
</div><!--container-->

<?php endif; ?>