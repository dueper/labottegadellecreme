<?php
$blocks_args = array('post_type'=>'blocks','posts_per_page'=>4);
$blocks = new WP_Query($blocks_args);
$i=1;
if($blocks->have_posts()): ?>
    <div class="home-bloks">
        <?php while($blocks->have_posts()): $blocks->the_post();
            $img   = get_field('immagine_blocco');
            $link  = get_field('link_blocco');
            switch($i) {
                case 1: ?>
        
                    <div class="block1">
                        <?php if($link['url']):
                            echo '<a href="'.$link['url'].'" target="'.$link['target'].'" title="'.the_title().'">';
                        endif; ?>
                            <div class="content content-top">
                                <figure class="left">
                                    <img src="<?php echo $img['sizes']['single-product']; ?>" class="img-responsive" />
                                </figure>
                                <figcaption class="right">
                                    <h1 class="centered-text"><?php the_title(); ?></h1>
                                </figcaption>
                            </div><!--content-->
                        <?php if($link['url']) echo '</a>'; ?>
                        
                <?php
                break;
                case 2: ?>
                    
                        <?php if($link['url']):
                            echo '<a href="'.$link['url'].'" target="'.$link['target'].'" title="'.the_title().'">';
                        endif; ?>
                            <div  class="content content-bottom">
                                <figure class="right">
                                    <img src="<?php echo $img['sizes']['single-product']; ?>" class="img-responsive" />
                                </figure>
                                <figcaption class="left">
                                    <h1 class="centered-text"><?php the_title(); ?></h1>
                                </figcaption>
                            </div><!--content-->
                        <?php if($link['url']) echo '</a>'; ?>
                    </div><!--block1-->
                        
                <?php
                break;
                case 3: ?>
            
                    <div  class="block2">
                        <?php if($link['url']):
                            echo '<a href="'.$link['url'].'" target="'.$link['target'].'" title="'.the_title().'">';
                        endif; ?>
                            <div class="content">
                                <h1 class="centered-text"><?php the_title(); ?></h1>
                                <figure>
                                    <img src="<?php echo $img['sizes']['single-product']; ?>" class="img-responsive" />
                                </figure>
                            </div><!--content-->
                        <?php if($link['url']) echo '</a>'; ?>
                    </div><!--block2-->
        
                <?php
                break;
                case 4: ?>
                    
                    <div class="block3">
                        <?php if($link['url']):
                            echo '<a href="'.$link['url'].'" target="'.$link['target'].'" title="'.the_title().'">';
                        endif; ?>
                            <figure class="content content-top">
                                <img src="<?php echo $img['sizes']['single-product']; ?>" class="img-responsive" />
                            </figure>
                            <figcaption class="content content-bottom">
                                <h1 class="centered-text"><?php the_title(); ?></h1>
                            </figcaption>
                        <?php if($link['url']) echo '</a>'; ?>
                    </div><!--block3-->
        
            <?php break;
            } ?>
        <?php $i++;
        endwhile; ?>
    </div><!--home-bloks-->
<?php endif; ?>