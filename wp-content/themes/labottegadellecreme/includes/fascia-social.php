<div class="socialbar">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
              <?php if ( is_user_logged_in() ) : ?>
              <?php get_template_part( 'includes/mailchimp' ); ?>
              <?php endif; ?>
              
            </div>
            <div class="col-md-6">
                    <?php
                    if(get_field('facebook','option')): 
                        echo '<a href="'.get_field('facebook','option').'" target="_blank"><span class="icon-facebook"></span></a>';
                    endif;
                    if(get_field('twitter','option')): 
                        echo '<a href="'.get_field('twitter','option').'" target="_blank"><span class="icon-twitter"></span></a>';
                    endif;
                    if(get_field('googleplus','option')): 
                        echo '<a href="'.get_field('googleplus','option').'" target="_blank"><span class="icon-google"></span></a>';
                    endif;
                    if(get_field('instagram','option')): 
                        echo '<a href="'.get_field('instagram','option').'" target="_blank"><span class="icon-instagram"></span></a>';
                    endif;
                    if(get_field('pinterest','option')): 
                        echo '<a href="'.get_field('pinterest','option').'" target="_blank"><span class="icon-pinterest"></span></a>';
                    endif;
                    ?>
            </div>
        </div><!--row-->
    </div><!--container-->
</div><!--socialbar-->