<?php if(get_field('slogan_home','option')): ?>
<div class="container">
    <div class="intro-text">
        <?php the_field('slogan_home','option'); ?>
    </div>
</div><!--container-->
<?php endif; ?>