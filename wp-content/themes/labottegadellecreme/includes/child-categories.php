<?php
$current_cat = get_queried_object()->term_id;
if($current_cat):
  $cats_arg = array(
                      'hide_empty'        => true,
                      'child_of'          => $current_cat
                  );
  $categories = get_terms('product_cat', $cats_arg);
  if($categories): ?>
      <div class="container home-categories">
        <div class="row">
          <?php foreach($categories as $cat):
              $link = get_term_link( $cat, 'product_cat' );
              $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
              $image = wp_get_attachment_image( $thumbnail_id, 'thumbnail' );
              ?>
              <div class="col-sm-6">
                  <a class="cat-content" href="<?php echo $link; ?>" title="<?php echo $cat->name; ?>">
                      <h1><?php echo $cat->name; ?></h1>
                      <hr class="sep" />
                      <h3><?php echo  $cat->description; ?></h3>
                      <figure>
                          <?php echo $image; ?>
                      </figure>
                  </a>
              </div>
          <?php endforeach; ?>
          </div><!--row-->
        <hr />
      </div><!--container-->
  <?php endif;
endif; ?>