<?php if(get_field('cosè') || get_field('per') || get_field('come_si_usa') || get_field('note') || get_field('ingredienti')): ?>
    <div class="tabber-prodotto-content">
        <div class="tab-head">
            <div class="container">
                <ul class="nav nav-tabs" role="tablist" id="myTab">
                    <?php if(get_field('cosè')): ?>
                        <li role="presentation" class="active"><a href="#cosè" aria-controls="cosè" role="tab" data-toggle="tab">Cos'è</a></li>
                    <?php endif; ?>
                    <?php if(get_field('per')): ?>
                        <li role="presentation"><a href="#per" aria-controls="per" role="tab" data-toggle="tab">Per</a></li>
                    <?php endif; ?>
                    <?php if(get_field('come_si_usa')): ?>
                        <li role="presentation"><a href="#comesiusa" aria-controls="comesiusa" role="tab" data-toggle="tab">Come si usa</a></li>
                    <?php endif; ?>
                    <?php if(get_field('note')): ?>
                        <li role="presentation"><a href="#note" aria-controls="note" role="tab" data-toggle="tab">Note</a></li>
                    <?php endif; ?>
                    <?php if(get_field('ingredienti')): ?>
                        <li role="presentation"><a href="#ingredienti" aria-controls="ingredienti" role="tab" data-toggle="tab">Ingredienti</a></li>
                    <?php endif; ?>
                </ul>
            </div><!--conteiner-->
        </div><!--tab-head-->
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="tab-content">
                        <?php if(get_field('cosè')): ?>
                            <div role="tabpanel" class="tab-pane fade in active" id="cosè"><?php the_field('cosè'); ?></div>
                        <?php endif; ?>
                        <?php if(get_field('per')): ?>
                            <div role="tabpanel" class="tab-pane fade" id="per"><?php the_field('per'); ?></div>
                        <?php endif; ?>
                        <?php if(get_field('come_si_usa')): ?>
                            <div role="tabpanel" class="tab-pane fade" id="comesiusa"><?php the_field('come_si_usa'); ?></div>
                        <?php endif; ?>
                        <?php if(get_field('note')): ?>
                            <div role="tabpanel" class="tab-pane fade" id="note"><?php the_field('note'); ?></div>
                        <?php endif; ?>
                        <?php if(get_field('ingredienti')): ?>
                            <div role="tabpanel" class="tab-pane fade" id="ingredienti"><?php the_field('ingredienti'); ?></div>
                        <?php endif; ?>
                    </div><!--tab-content-->
                </div><!--col-md-10-->
            </div><!--row-->
        </div><!--container-->
    </div><!--tabber-prodotto-content-->
<?php endif; ?>