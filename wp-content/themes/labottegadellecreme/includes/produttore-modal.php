<!-- Modal -->
<div class="modal modal-produttore fade" id="modal-<?php the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

      <div class="modal-body">
        <figure><?php the_post_thumbnail('medium',array('class'=>'img-responsive aligncenter')); ?></figure>
        <h4 class="modal-title" id="myModalLabel"><?php the_title(); ?></h4>
        <?php the_content(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-close" data-dismiss="modal"><?php _e('Chiudi'); ?></button>
        <?php $link = get_field('sito_web');
        if($link): 
            echo '<a href="'.$link['url'].'" target="_blank" class="btn btn-link">'. __('Vai al sito').'</a>';
        endif; ?>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->