<?php
$slide_args = array('post_type'=>'slides','posts_per_page'=>-1);
$slider = new WP_Query($slide_args);
if($slider->have_posts()): ?>
    <div class="intro-slider flexslider">
        <div class="frame-top"></div>
        <div class="frame-right"></div>
        <div class="frame-bottom"></div>
        <div class="frame-left"></div>
        <ul class="slides">
            <?php while($slider->have_posts()): $slider->the_post();
                $image = get_field('immagine_slide');?>
                <li class="item" style="background-image:url('<?php echo $image['sizes']['bg-fascia'] ?>');" data-adaptive-background='1' data-ab-css-background='1'>
                    <div class="slide-text" style="top:<?php the_field('margin_top'); ?>%">
                        <?php
                        if(get_field('titolo_visualizzato')):
                            $link = get_field('link');
                            if($link['url']) echo '<a href="'.$link['url'].'" target="'.$link['target'].'">';
                                echo '<h1>'.get_field('titolo_visualizzato').'</h1>';
                            if($link['url']) echo '</a>';
                        endif;
                        ?>
                        
                        <?php
                        $link = get_field('url');
                        if($link['url']):
                            echo '<a href="'.$link['url'].'" target="'.$link['target'].'" class="slide-link">'.$title.'</a>';
                        endif; ?>
                        
                    </div>
                </li><!--item-->
            <?php endwhile; ?>
        </ul>
    </div><!--intro-slider-->
<?php endif; wp_reset_query(); ?>