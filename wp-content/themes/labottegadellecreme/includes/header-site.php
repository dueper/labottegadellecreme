<?php $page_title = get_the_title(); ?>

<?php
$slide_args = array('post_type'=>'slides','posts_per_page'=>1);
$slider = new WP_Query($slide_args);
if($slider->have_posts()): ?>
    <div class="intro-slider">
        <div class="frame-top"></div>
        <div class="frame-right"></div>
        <div class="frame-bottom"></div>
        <div class="frame-left"></div>
       
        <?php while($slider->have_posts()): $slider->the_post();
            $image = get_field('immagine_slide');?>
            <div class="bg-header" style="background-image:url('<?php echo $image['sizes']['bg-fascia'] ?>');" data-adaptive-background='1' data-ab-css-background='1'>
                <h1 class="page-title"><?php echo $page_title; ?></h1>
            </div><!--item-->
        <?php endwhile; ?>
    </div><!--intro-head-->
<?php endif; wp_reset_query(); ?>