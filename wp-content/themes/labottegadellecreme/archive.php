<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @author Dueper Design
 * @package Dueper Theme
 */

get_header(); ?>
	<div class="container">
		<?php if ( have_posts() ) : ?>

            <header class="page-header">
                <h1 class="page-title">
                    <?php
                        if ( is_category() ) :
                            single_cat_title();

                        elseif ( is_tag() ) :
                            single_tag_title();

                        elseif ( is_author() ) :
                            /* Queue the first post, that way we know
                             * what author we're dealing with (if that is the case).
                            */
                            the_post();
                            printf( __( 'Author: %s', 'upbootwp' ), '<span class="vcard">' . get_the_author() . '</span>' );
                            /* Since we called the_post() above, we need to
                             * rewind the loop back to the beginning that way
                             * we can run the loop properly, in full.
                             */
                            rewind_posts();

                        elseif ( is_day() ) :
                            printf( __( 'Day: %s', 'upbootwp' ), '<span>' . get_the_date() . '</span>' );

                        elseif ( is_month() ) :
                            printf( __( 'Month: %s', 'upbootwp' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );

                        elseif ( is_year() ) :
                            printf( __( 'Year: %s', 'upbootwp' ), '<span>' . get_the_date( 'Y' ) . '</span>' );

                        elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
                            _e( 'Asides', 'upbootwp' );

                        elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
                            _e( 'Images', 'upbootwp');

                        elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
                            _e( 'Videos', 'upbootwp' );

                        elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
                            _e( 'Quotes', 'upbootwp' );

                        elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
                            _e( 'Links', 'upbootwp' );

                        else :
                            _e( 'Archives', 'upbootwp' );

                        endif;
                    ?>
                </h1>
            </header><!-- .page-header -->
            <div class="row">
				<div class="col-md-8">
					<?php while ( have_posts() ) : the_post(); ?>             
                        <article <?php post_class(); ?>>
                            <date><?php echo get_the_date(); ?></date>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                            	<h1 class="single-title"><?php the_title(); ?></h1>
                            </a>
                            <?php the_post_thumbnail('thumbnail',array('class'=>'img-responsive')); ?>
                            <?php the_excerpt(); ?>
                        </article><!--post_class-->
                    <?php endwhile; ?>
                </div><!-- .col-md-8 -->
                <div class="col-md-4">
                	<?php get_sidebar(); ?>
                </div><!--col-md-4-->
            </div><!-- .row -->
    	<?php endif; ?>
	</div><!-- .container -->
<?php get_footer(); ?>
